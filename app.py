from flask import Flask,request,abort, signals
from linebot import (LineBotApi,WebhookHandler)
from linebot.exceptions import(InvalidSignatureError)
from linebot.models import*

app=Flask(__name__)
line_bot_api=LineBotApi('fPolCnLBjKyL9rESNVEuLhU29ZMWiIQjKqjyc7j4EiPTHXpsXHEA/OKGVzAjpl6yHk7yFfJ/kuCpFYbCCuhVu86MM5dhsrRTdJ0CYaT0eIVOJI2kSl65RGEiOuOndy983Ruzrp/4BX5U1AwKlKNw2wdB04t89/1O/w1cDnyilFU=')
handler=WebhookHandler('a11d2ef353cf57daeefea04c46063b8c')

@app.route("/callback",methods='[POST]')
def callback():
    signature=request.headers['X-Line-Signature']
    body=request.get_data(as_text=True)
    app.logger.info("Request body: "+body)
    try:
        handler.handle(body,signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'

